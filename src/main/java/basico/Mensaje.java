package basico;


import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;

@Path("/mensaje")
public class Mensaje {
	 
	@GET
	@Path("/{nombre}/{apellido}")
	public Response imprimirMessage(@PathParam("nombre") String nom, @PathParam("apellido") String ape) {
	String salida = "Restful Web Service, bienvenido " + nom + " " + ape;
	return Response.status(200).entity(salida).build();
	}
	
}
