package basico;


import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

@Path("/saludo")
public class HolaResource {
    @GET
    @Produces("text/plain")
    public String hello() {
        return "Hola, mundo rest!";
    }
}