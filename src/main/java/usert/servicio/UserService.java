package usert.servicio;

import usert.entidad.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UserService {

    private static final List<User> users = new ArrayList<User>();

    public List<User> fetchAll() {
        return users;
    }

    public User fetchBy(long id) {
        for (User user2 : fetchAll()) {
            if (id == user2.getId()) {
                return user2;
            }
        }
        return null;
    }

    public void create(User user) {
        users.add(user);
    }

    public void update(long id, User user) {

        Iterator<User> it = users.iterator();

        while (it.hasNext()) {
            User use = it.next();
            if (use.getId() == id) {
                it.remove();
                break;
            }
        }

        users.add(user);

    }

    public void delete(long id) {

        Iterator<User> it = users.iterator();

        while (it.hasNext()) {
            User use = it.next();
            if (use.getId() == id) {
                it.remove();
                break;
            }
        }

    }

}
