package usert.servicio;

import usert.entidad.Socio;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SocioService {
    
    private static final List<Socio> listSocio = new ArrayList<Socio>();
    
    public List<Socio> consultarSocio() {
        return listSocio;
    }
    
    public Socio consultarSocio(int numSocio) {
        
        for (Socio socio : listSocio) {
            if (numSocio == socio.getNumeroSocio().intValue()) {
                return socio;
            }
        }
        
        return null;
    }
    
    public void crearSocio(Socio socioNuevo) {
        
        for (Socio socio : listSocio) {
            if (socioNuevo.getNumeroSocio().intValue() == socio.getNumeroSocio().intValue()) {
                return;
            }
        }
        listSocio.add(socioNuevo);
    }
    
    public void actualizarSocio(int numSocio, Socio socioModificado) {
        
        Iterator<Socio> it = listSocio.iterator();
        
        while (it.hasNext()) {
            Socio socio = it.next();
            if (socio.getNumeroSocio().intValue() == numSocio) {
                it.remove();
                break;
            }
        }
        
        listSocio.add(socioModificado);
        
    }
    
    public void eliminarSocio(int numSocio) {
        
        Iterator<Socio> it = listSocio.iterator();
        
        while (it.hasNext()) {
            Socio socio = it.next();
            if (socio.getNumeroSocio().intValue() == numSocio) {
                it.remove();
                break;
            }
        }
        
    }
    
}
