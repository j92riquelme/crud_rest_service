package usert.entidad;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement(name = "datosSocio")
public class Socio {
    private Integer numeroSocio;
    private String documento;
    private Long aporte;
    private String calificacionCliente;
    private Character tipoSocio;

    public Socio() {
    }

    @XmlElement(name = "socio")
    public Integer getNumeroSocio() {
        return numeroSocio;
    }

    public void setNumeroSocio(Integer numeroSocio) {
        this.numeroSocio = numeroSocio;
    }

    @XmlElement(name = "documentoSocio")
    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @XmlElement(name = "aporteSocio")
    public Long getAporte() {
        return aporte;
    }

    public void setAporte(Long aporte) {
        this.aporte = aporte;
    }

    @XmlElement(name = "calificacionSocio")
    public String getCalificacionCliente() {
        return calificacionCliente;
    }

    public void setCalificacionCliente(String calificacionCliente) {
        this.calificacionCliente = calificacionCliente;
    }

    @XmlElement(name = "tipoSocio")
    public Character getTipoSocio() {
        return tipoSocio;
    }

    public void setTipoSocio(Character tipoSocio) {
        this.tipoSocio = tipoSocio;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.numeroSocio);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Socio other = (Socio) obj;
        if (!Objects.equals(this.numeroSocio, other.numeroSocio)) {
            return false;
        }
        return true;
    }

}
