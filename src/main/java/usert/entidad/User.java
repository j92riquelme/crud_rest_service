package usert.entidad;


import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({"name", "email", "id"})
public class User {

    @JsonAlias({"identificador"})
    private long id;

    @JsonAlias({"nombre"})
    private String name;

    @JsonAlias({"correo" })
    private String email;

    public User() {
    }

    public User(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    @JsonGetter("idUsuario")
    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    @JsonGetter("nombreCompleto")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter("correoElectronico")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
