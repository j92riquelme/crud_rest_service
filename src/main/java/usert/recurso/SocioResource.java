package usert.recurso;


import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import usert.entidad.Socio;
import usert.servicio.SocioService;


import java.util.List;

@Path("/socios")
public class SocioResource {
    
    private SocioService socioServices = new SocioService();

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<Socio> consultarSoc() {
        return socioServices.consultarSocio();
    }
 
    @POST
    @Path("/socio")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Response crearSoc(Socio nuevoSocio) {
        socioServices.crearSocio(nuevoSocio);
        return Response.status(Response.Status.CREATED).build();
    }
    
    @PUT
    @Path("/socio/{numSocio}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response actualizarSoc(@PathParam("numSocio") int numSocio, Socio socioModificado) {
        socioServices.actualizarSocio(numSocio, socioModificado);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/socio/{numSocio}")
    public Response eliminarSoc(@PathParam("numSocio") int numSocio) {
        socioServices.eliminarSocio(numSocio);
        return Response.status(Response.Status.OK).build();
    }
    
    @GET
    @Path("/socio/{numSocio}")
    @Produces(MediaType.APPLICATION_XML)
    public Response consultarSoc(@PathParam("numSocio") int numeSocio) {
        Socio socio = socioServices.consultarSocio(numeSocio);
        return Response.ok().entity(socio).build();
    }
    
    
}
