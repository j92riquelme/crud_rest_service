
package usert.recurso;


import usert.entidad.User;
import usert.servicio.UserService;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;

@Path("users")
public class UserResource {

    private UserService userService = new UserService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> fetchAll() {
        return userService.fetchAll();
    }

    @GET
    @Path("user/{id}/{edad}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id, @PathParam("edad") int edad) {
        User u = userService.fetchBy(id);
        System.out.println("Edad" + edad);
        return Response.ok().entity(u).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(User user) {
        userService.create(user);
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/user/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, User user) {
        userService.update(id, user);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/user/{id}")
    public Response delete(@PathParam("id") long id) {
        userService.delete(id);
        return Response.status(Response.Status.OK).build();
    }

}
