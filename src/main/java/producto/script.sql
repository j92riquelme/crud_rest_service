-- Database: restdb

-- DROP DATABASE IF EXISTS restdb;

CREATE DATABASE restdb
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


-- SEQUENCE: public.productos_id_producto_seq

-- DROP SEQUENCE public.productos_id_producto_seq;

CREATE SEQUENCE public.productos_id_producto_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.productos_id_producto_seq
    OWNER TO postgres;


-- Table: public.productos

-- DROP TABLE public.productos;

CREATE TABLE IF NOT EXISTS public.productos
(
    id_producto integer NOT NULL DEFAULT nextval('productos_id_producto_seq'::regclass),
    nombre character varying(50) COLLATE pg_catalog."default" NOT NULL,
    precio integer NOT NULL,
    proveedor character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT productos_pkey PRIMARY KEY (id_producto)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.productos
    OWNER to postgres;