package producto.db;

import java.sql.Connection;
import java.sql.DriverManager;
public class BaseDato {
	private Connection conexion=null;
    private String user = "postgres";
    private String pass = "ROOT";
    private String bd = "jdbc:postgresql://localhost:5433/restdb";
     
    public Connection conectar() throws Exception {
        Class.forName("org.postgresql.Driver");
        conexion = DriverManager.getConnection(bd,user,pass);
        return conexion;        
    }
}
