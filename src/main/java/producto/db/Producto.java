package producto.db;

public class Producto {
    private Integer id;
    private String nombre;
    private Integer precio;
    private String proveedor;

    public Producto() {
        super();
    }

    public Producto(int id, String nombre, int precio, String proveedor) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.proveedor = proveedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return  "identificador: " + id +
                ", nombre: " + nombre +
                ", precio: " + precio +
                ", proveedor:" + proveedor ;
    }
}
