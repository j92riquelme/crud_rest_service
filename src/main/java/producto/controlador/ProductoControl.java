package producto.controlador;


import producto.db.Producto;
import producto.db.BaseDato;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductoControl {

    public ArrayList<Producto> listarTodosProductos() throws Exception {

        ArrayList<Producto> listaDeProductos = new ArrayList<Producto>();
        BaseDato bd = new BaseDato();

        Connection conexion = bd.conectar();
        Statement st = conexion.createStatement();
        String sql = "SELECT * FROM productos ";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            Producto producto = new Producto();
            producto.setId(rs.getInt("id_producto"));
            producto.setNombre(rs.getString("nombre"));
            producto.setPrecio(rs.getInt("precio"));
            producto.setProveedor(rs.getString("proveedor"));
            listaDeProductos.add(producto);
        }
        st.close();
        conexion.close();
        return listaDeProductos;
    }

    public Producto listarProductos(int producto_id) throws Exception {

        BaseDato bd = new BaseDato();

        Connection conexion = bd.conectar();

        String sql = "SELECT * FROM productos where id_producto = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sql);
        sentencia.setInt(1, producto_id);
        ResultSet rs = sentencia.executeQuery();
        Producto producto = new Producto();
        while (rs.next()) {
            producto.setId(rs.getInt("id_producto"));
            producto.setNombre(rs.getString("nombre"));
            producto.setPrecio(rs.getInt("precio"));
            producto.setProveedor(rs.getString("proveedor"));
        }
        sentencia.close();
        conexion.close();
        return producto;
    }

    public boolean agregarProducto(String nombre, int precio, String proveedor) {

        BaseDato bd = new BaseDato();

        Connection conexion;
        try {
            conexion = bd.conectar();
            Statement st = conexion.createStatement();
            String sql = "INSERT INTO  public.productos (nombre, precio, proveedor) "
                    + "    VALUES (?, ?, ?)";
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombre);
            sentencia.setInt(2, precio);
            sentencia.setString(3, proveedor);

            int resultado = sentencia.executeUpdate();
            sentencia.close();
            if (resultado > 0)
                return true;

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return false;
    }

    public int borrarProducto(int id) {
        BaseDato bd = new BaseDato();

        Connection conexion;
        try {
            conexion = bd.conectar();
            String sql = "DELETE FROM public.productos WHERE id_producto=?";
            PreparedStatement sentencia = conexion.prepareStatement(sql);

            sentencia.setInt(1, id);
            int resultado = sentencia.executeUpdate();
            sentencia.close();
            return resultado;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int update(int producto_id, int precio, String nombre, String proveedor) {
        BaseDato bd = new BaseDato();

        Connection conexion;
        try {
            conexion = bd.conectar();
            String sql = "UPDATE public.productos SET precio=? ,nombre=? ,proveedor=? WHERE id_producto=?";
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, precio);
            sentencia.setString(2, nombre);
            sentencia.setString(3, proveedor);
            sentencia.setInt(4, producto_id);

            int resultado = sentencia.executeUpdate();
            sentencia.close();
            return resultado;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }
}
