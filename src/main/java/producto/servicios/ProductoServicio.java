package producto.servicios;

import java.util.ArrayList;

import com.google.gson.Gson;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import producto.db.Producto;
import producto.controlador.ProductoControl;
import producto.db.Entidad;


@Path("/producto")
public class ProductoServicio {

    @GET
    @Path("/lista/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public String listarProducto(@PathParam("param") int valor) throws Exception {
        Gson gson = new Gson();
        ProductoControl productoControl = new ProductoControl();
        Producto producto = productoControl.listarProductos(valor);
        return gson.toJson(producto);
    }

    @GET
    @Path("/listar")
    @Produces({MediaType.APPLICATION_JSON})
    public String listarTodosProductos() throws Exception {
        Gson gson = new Gson();
        ProductoControl productoControl = new ProductoControl();
        ArrayList<Producto> listaDeProductos = productoControl.listarTodosProductos();
        return gson.toJson(listaDeProductos);
    }

    @POST
    @Path("/agregar")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregarProducto(String json) {
        ProductoControl productoControl = new ProductoControl();
        Gson gson = new Gson();
        System.out.println(json);
        Entidad producto = gson.fromJson(json, Entidad.class);
        boolean respuesta = productoControl.agregarProducto(producto.getNombre(), Integer.parseInt(producto.getPrecio()), producto.getProveedor());
        if (respuesta)
            return Response.status(200).entity("Producto agregado correctamente").build();
        return Response.status(407).entity("No se pudo agregar el Producto").build();

    }

    @DELETE
    @Path("/eliminar/{param}")
    public Response eliminarProducto(@PathParam("param") int valor) throws Exception  {
        ProductoControl productoControl = new ProductoControl();
        Producto productoTemporal = productoControl.listarProductos(valor);
        int respuesta = productoControl.borrarProducto(valor);
        if (respuesta > 0)
            return Response.status(200).entity(respuesta + " Producto eliminado -> " + productoTemporal.toString()).build();
        return Response.status(200).entity(" Ningun producto eliminado").build();
    }

    @PUT
    @Path("/actualizar/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response actualizarProducto(String json, @PathParam("param") int valor) throws Exception {
        ProductoControl productoControl = new ProductoControl();
        Gson gson = new Gson();
        Entidad datos = gson.fromJson(json, Entidad.class);
        int respuesta = productoControl.update(valor, Integer.parseInt(datos.getPrecio()), datos.getNombre(), datos.getProveedor());
        if (respuesta > 0) {
            Producto productoTemporal = productoControl.listarProductos(valor);
            return Response.status(200).entity(respuesta + " "
                    + "Producto actualizado correctamente -> " +  productoTemporal.toString()).build();
        }
        return Response.status(200).entity("Ningun dato actualizado").build();

    }

}
