package calculadora;


import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

@Path("/calculadora")
public class Calculadora {

    @GET
    @Path("/sumar")
    public String suma (@QueryParam("num1") int numer1, @QueryParam("num2")int numer2){
        return String.valueOf(numer1 + numer2);
    }

    @GET
    @Path("/restar")
    public String resta (@QueryParam("num1") int numero1, @QueryParam("num2") int numero2) {
        return String.valueOf(numero1 - numero2);
    }

    @GET
    @Path("/multiplicar")
    public String multiplica(@QueryParam("num1") int numer1, @QueryParam("num2") int numer2) {
        return String.valueOf(numer1 * numer2);
    }

    @GET
    @Path("/dividir")
    public String divide(@QueryParam("num1") int numero1, @QueryParam("num2") int numero2) {
        return String.valueOf(numero1 / numero2);
    }


}
